#include <stdio.h>
#include <stdlib.h>
#include "board.h"

// CHANGE THESE LINES IF YOU WOULD LIKE TO CUSTOMIZE.
// boardSize:		the board size
// winLength:		the number of things in a row you need to win
// botLookahead:	how many moves the bot looks ahead.
#define boardSize 5
#define winLength 4
#define botLookahead 4

int getInt( int n, int lowerBound, int upperBound ) {
	/*
	read n bytes from stdin and convert them into a integer,
	if !( lowerBound <= integer < upperBound ) try again
	*/
	
	int out;
	char * buffer = malloc( sizeof(char)*n );
	while ( 1 ) {
		
		fgets( buffer, n, stdin );
		out = atoi( buffer );
		if ( lowerBound <= out && out < upperBound ) {
			break;
		}
		
	}
	
	return out;
}

int main( int argc, char ** argv, char ** envp ) {
	
	struct board * b = newBoard( boardSize );
	int currPlayer = O;
	int botPlayer = X;
	int x, y;
	while (1) {
		
		system( "clear" );
		printf("| Board size: %d\n", boardSize);
		printf("| Chain length to win: %d\n", winLength);
		printf("| Board:\n");
		printBoard( b, 2 );
		
		int win = winner( b, winLength );
		if ( win != -1 ) {
			printf("| Done.\n");
			if ( win==O ) {
				printf("| Winner: O\n");
			} else if ( win==X ) {
				printf("| Winner: X\n");
			} else {
				printf("| Winner: tie\n");
			}
			break;
		}
		
		printf("| Current player: ");
		if ( currPlayer==O ) { printf("O\n"); }
		else if ( currPlayer==X ) { printf("X\n"); }
		
		if ( currPlayer != botPlayer ) {
			while ( 1 ) {
				printf("| x: "); x = getInt( 3, 1, boardSize+1 ); x--;
				printf("| y: "); y = getInt( 3, 1, boardSize+1 ); y--;
				
				if ( x < 0 || y < 0 || x >= b->size || y >= b->size ) {
					printf("| Error: invalid coordinate.\n");
					continue;
				}
				if ( b->grid[ y*b->size + x ] != 0 ) {
					printf("| Error: that spot is already occupied.\n");
					continue;
				}
				
				break;
			}
			
			putValue( b, currPlayer, x, y );
		} else {
			
			// find a good move for the bot
			printf("| The bot is thinking..\n");
			int moveIndex = findMove( b, botLookahead, botPlayer, winLength );
			b->grid[moveIndex] = botPlayer;
			
		}
		
		currPlayer = otherPlayer( currPlayer );
		
	}
	
	return 0;
}
