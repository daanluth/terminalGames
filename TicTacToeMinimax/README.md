# TicTacToeMinimax

## Usage:
#### first time:
you can change the first few lines of main.c if you<br/>
would like to set things like the board size<br/>
$ make all<br/>
#### after that:
$ ./main<br/>

### SHA256 hashes:
6d5c57cc5b433444b9efde1404b1575287626fd93c77bcd0a2e72177e917734f  board.h<br/>
88aa51be186954f336f74c7085aae88d30f6e4652e4be16c51ed1db8dea81972  main.c<br/>

