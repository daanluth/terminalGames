#ifndef __BOARD_H__
#define __BOARD_H__

#include <stdlib.h>
#include <stdio.h>

#define X 1
#define O 2

struct board {
	int size;
	unsigned char * grid;
};
void printBoard( struct board * b, int indent ) {
	
	for ( int j=0; j<indent+1; j++ ) { printf(" "); }
	for ( int i=0; i<b->size; i++ ) {
		printf(" %d", i+1);
	}
	printf("\n");
	for ( int j=0; j<indent; j++ ) { printf(" "); }
	printf("1");
	for ( int i=0; i<(b->size*b->size); i++ ) {
		if ( b->grid[ i ] == 0 ) {
			printf(" -");
		} else if ( b->grid[ i ] == O ) {
			printf(" O");
		} else if ( b->grid[ i ] == X ) {
			printf(" X");
		}
		if ( ((i+1) % b->size) == 0 && i > 0 ) {
			printf("\n");
			if ( i!=(b->size*b->size)-1 ) {
				for ( int j=0; j<indent; j++ ) { printf(" "); }
				printf("%d", ( i / b->size )+2);
			}
		}
	}
	
}

struct board * newBoard( int size ) {
	
	struct board * out = (struct board *) malloc( sizeof(struct board) );
	out->size = size;
	out->grid = (unsigned char *) malloc( sizeof(unsigned char)*(size*size) );
	
	return out;
}
struct board * copyBoard( struct board * b ) {
	struct board * out = (struct board *) malloc( sizeof(struct board) );
	out->size = b->size;
	out->grid = (unsigned char *) malloc( sizeof(unsigned char)*(b->size*b->size) );
	// copy the grid
	for ( int i=0; i<(b->size*b->size); i++ ) {
		out->grid[i] = b->grid[i];
	}
	
	return out;
}
void putValue( struct board * b, unsigned char value, int x, int y ) {
	b->grid[ (y*b->size) + x ] = value;
}
int winner( struct board * b, int winLength ) {
	
	// check the rows
	int isTie = 1;
	unsigned char curr;
	int found;
	for ( int y = 0; y < b->size; y++ ) {
		found = 0; curr = 0xff;
		for ( int x = 0; x<b->size; x++ ) {
			
			if ( b->grid[ (y*b->size) + x ] == 0 ) {
				// this cant be a tie since there are empty spaces left
				isTie = 0;
			}
			
			if ( b->grid[ (y*b->size) + x ] == curr ) {
				found++;
				if ( found == winLength ) {
					if (curr != 0) {
						return curr;
					}
				}
			} else {
				curr = b->grid[ (y*b->size) + x ];
				found = 1;
			}
			
		}
	}
	// check the columns
	for ( int x = 0; x < b->size; x++ ) {
		found = 0; curr = 0xff;
		for ( int y = 0; y < b->size; y++ ) {
			
			if ( b->grid[ (y*b->size) + x ] == curr ) {
				found++;
				if ( found == winLength ) {
					if (curr != 0) {
						return curr;
					}
				}
			} else {
				curr = b->grid[ (y*b->size) + x ];
				found = 1;
			}
			
		}
	}
	found = 0; curr = 0xff;
	// check the diagonals
	for ( int startX=-(b->size - winLength); startX <= (b->size - winLength); startX++ ) {
		int x = startX;
		int y = 0;
		// top right to bottom left
		found = 0; curr = 0xff;
		while ( x < b->size && y < b->size ) {
			
			if ( x >= 0 ) {
				if ( b->grid[ (y*b->size) + x ] == curr ) {
					found++;
					if ( found == winLength ) {
						if (curr != 0) {
							return curr;
						}
					}
				} else {
					curr = b->grid[ (y*b->size) + x ];
					found = 1;
				}
			}
			
			x++;
			y++;
		}
		// bottom right to top left
		x = startX;
		y = b->size - 1;
		found = 0; curr = 0xff;
		while ( x < b->size && y < b->size ) {
			
			if ( x >= 0 ) {
				if ( b->grid[ (y*b->size) + x ] == curr ) {
					found++;
					if ( found == winLength ) {
						if (curr != 0) {
							return curr;
						}
					}
				} else {
					curr = b->grid[ (y*b->size) + x ];
					found = 1;
				}
			}
			
			x++;
			y--;
		}
	}
	
	if ( isTie ) {
		return 0;
	} else {
		// nobody has won yet
		return -1;
	}
}
int boardFull( struct board * b ) {
	for ( int i=0; i<(b->size * b->size); i++ ) {
		if ( b->grid[i] == 0 ) {
			return 0;
		}
	}
	return 1;
}

int otherPlayer( int currPlayer ) {
	if ( currPlayer == O ) {
		return X;
	} else if ( currPlayer == X ) {
		return O;
	}
	return O;
}

int findMove_minimax( struct board * b, int depth, int maximize, int alpha, int beta, int player, int winLength ) {
	
	int w = winner( b, winLength );
	if ( w != -1 || depth==0 ) {
		if ( w==player ) {
			// a winner is you
			return 1 * (depth+1);
		} else if ( w==0 ) {
			// tie
			return 0;
		} else if ( w==-1 ) {
			// nobody has won yet
			return 0;
		} else {
			// the other guy or gal won..
			return -1 * (depth+1);
		}
	}
	
	int score;
	if ( maximize ) {
		
		score = -100000;
		for ( int i=0; i<(b->size*b->size); i++ ) {
			
			// can we place a piece here?
			if (b->grid[i] != 0) {
				// no we can't
				continue;
			}
			
			// try placing a piece here
			b->grid[i] = player;
			int testScore = findMove_minimax(
				b, depth-1, 0, alpha, beta,
				otherPlayer(player), winLength
			);
			// undo the move
			b->grid[i] = 0;
			if ( testScore > score ) {
				score = testScore;
			}
			// alpha-beta pruning stuff
			if ( alpha < score ) {
				alpha = score;
			}
			if ( alpha >= beta ) {
				break;
			}
			
		}
		
	} else {
		
		score = 100000;
		for ( int i=0; i<(b->size*b->size); i++ ) {
			
			// can we place a piece here?
			if (b->grid[i] != 0) {
				// no we can't
				continue;
			}
			
			// try placing a piece here
			b->grid[i] = player;
			int testScore = findMove_minimax(
				b, depth-1, 1, alpha, beta,
				otherPlayer(player), winLength
			);
			// undo the move
			b->grid[i] = 0;
			if ( testScore < score ) {
				score = testScore;
			}
			// alpha-beta pruning stuff
			if ( beta > score ) {
				beta = score;
			}
			if ( alpha >= beta ) {
				break;
			}
			
		}
		
	}
	
	return score;
}
int findMove( struct board * b, int depth, int player, int winLength ) {
	
	int bestIndex = -1;
	int bestScore = -10;
	for ( int i=0; i<(b->size*b->size); i++ ) {
		
		// can we place a piece here?
		if (b->grid[i] != 0) {
			// no we can't
			continue;
		}
		
		// try placing a piece here
		b->grid[i] = player;
		int win = winner( b, winLength );
		if ( win > 0 ) {
			// we win if we do this, so we dont have to look further
			return i;
		}
		int testScore = findMove_minimax(
			b, depth, 0, -10, 10,
			otherPlayer(player), winLength
		);
		//printf("place at %d, %d = %d\n", (i%b->size)+1, (i/b->size)+1, testScore);
		// undo the move
		b->grid[i] = 0;
		if ( testScore > bestScore ) {
			bestScore = testScore;
			bestIndex = i;
		}
		
	}
	
	return bestIndex;
}

#endif
