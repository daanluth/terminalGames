#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>

int getInt( int n, int lowerBound, int upperBound ) {
	/*
	read n bytes from stdin and convert them into a integer,
	if !( lowerBound <= integer < upperBound ) try again
	*/
	
	int out;
	char * buffer = malloc( sizeof(char)*n );
	while ( 1 ) {
		
		fgets( buffer, n, stdin );
		out = atoi( buffer );
		if ( lowerBound <= out && out < upperBound ) {
			break;
		}
		
	}
	
	return out;
}

struct board {
	int size;
	int totalsize;
	int * grid;
	int * updateGrid;
};

void doUpdates( struct board * b ) {
	for ( int i=0; i<b->totalsize; i++ ) {
		b->grid[i] = b->updateGrid[i];
	}
}
void calcUpdates( struct board * b ) {
	
	int nAround, testX, testY;
	for ( int y=0; y<b->size; y++ ) {
		for ( int x=0; x<b->size; x++ ) {
			
			nAround = 0;
			for ( int dy=-1; dy<=1; dy++ ) {
				testY = y+dy;
				if ( testY<0 || testY>=b->size ) { continue; }
				for ( int dx=-1; dx<=1; dx++ ) {
					if ( dx==0 && dy==0 ) { continue; }
					testX = x+dx;
					if ( testX<0 || testX>=b->size ) { continue; }
					
					if ( b->grid[ testY*b->size + testX ] == 1 ) {
						nAround++;
					}
				}
			}
			
			/*
			Any live cell with fewer than two live neighbours dies, as if by underpopulation.
			Any live cell with two or three live neighbours lives on to the next generation.
			Any live cell with more than three live neighbours dies, as if by overpopulation.
			Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
			*/
			int changeIndex = (y * b->size) + x;
			if ( nAround==2 || nAround==3 ) {
				if ( b->grid[ changeIndex ]==1 || nAround==3 ) {
					b->updateGrid[ changeIndex ] = 1;
				} else {
					b->updateGrid[ changeIndex ] = 0;
				}
			} else {
				b->updateGrid[ changeIndex ] = 0;
			}
			
		}
	}
	
}
void update( struct board * b ) {
	calcUpdates( b );
	doUpdates( b );
}

void printBoard( struct board * b ) {
	for ( int i=0; i<b->totalsize; i++ ) {
		if ( b->grid[i] == 0 ) {
			printf("__");
		} else {
			printf("\u2588\u2588");
		}
		if ( i>0 && ((i+1)%b->size)==0 ) {
			printf("\n");
		}
	}
}
struct board * newBoard( int size ) {
	struct board * out = ( struct board * ) malloc( sizeof(struct board) );
	out->size = size;
	out->totalsize = size*size;
	out->grid = (int *) malloc( sizeof(int) * out->totalsize );
	out->updateGrid = (int *) malloc( sizeof(int) * out->totalsize );
	return out;
}

int main( int argc, char ** argv, char ** envp ) {
	
	struct board * b;
	
	printf("Load grid config from file? [Y/n] ");
	char ans = fgetc( stdin );
	while ( fgetc( stdin )!='\n' ) {} // clear the rest of stdin
	if ( ans=='Y' || ans=='y' ) {
		printf("Config files:\n");
		DIR *dir;
		struct dirent *ent;
		if ((dir = opendir ("saves/")) != NULL) {
			// list the files
			while ((ent = readdir (dir)) != NULL) {
				if ( ent->d_name[0] == '.' ) { continue; }
				printf ("- '%s'\n", ent->d_name);
			}
			closedir (dir);
		} else {
			/* could not open directory */
			printf("Could not list config files.\n");
		}
		// get the file name
		while ( 1 ) {
			char fileName[32] = {0};
			char fullFileName[38] = "saves/";
			
			printf("file name: ");
			fgets( fileName, 32, stdin );
			for ( int i=0; i<32; i++ ) {
				if ( fileName[i]=='\n' ) { break; }
				fullFileName[i+6] = fileName[i];
			}
			// open the file
			FILE * configFile = fopen( fullFileName, "r" );
			if ( configFile == NULL ) {
				printf("Error, Could not open file,\nare you sure the filename was typed in correctly?\n");
				continue;
			}
			
			// read the config
			char c = '\0';
			char sizeBuffer[5] = {0};
			for ( int i=0; i<5; i++ ) {
				c = fgetc( configFile );
				if ( c==':' || c==EOF ) { break; }
				sizeBuffer[i] = c;
			}
			if ( c==EOF ) {
				printf("Parsing Error, no grid data in file.\n");
				continue;
			}
			int size = atoi( sizeBuffer );
			if ( size==0 ) {
				printf("Parsing Error, invalid size in file.\n");
				continue;
			}
			b = newBoard( size );
			for ( int i=0; i<size*size; i++ ) {
				c = fgetc( configFile );
				if ( c == EOF ) {
					break;
				}
				if ( c=='0' ) {
					b->grid[i] = 0;
				} else if ( c=='1' ) {
					b->grid[i] = 1;
				}
			}
			if ( c == EOF ) {
				printf("Parsing Error, not enough grid data.\n");
				continue;
			}
			// done setting up
			break;
		}
		
	} else {
		// edit mode
		system("clear");
		printf("| Creating new grid.\n| grid size: ");
		b = newBoard( getInt( 5, 0, 50 ) );
	}
	
	while ( 1 ) { 
		
		system("clear"); printBoard( b );
		printf("| Edit Mode, enter nothing to stop editing\n");
		
		int x, y;
		printf("| x: "); x = getInt( 5, 0, b->size+1 ); x--;
		printf("| y: "); y = getInt( 5, 0, b->size+1 ); y--;
		
		if ( x == -1 && y == -1 ) {
			break;
		}
		if ( x < 0 || y < 0 ) {
			continue;
		}
		
		// switch it from on to off or the other way around
		b->grid[ y*b->size + x ] = 1 - b->grid[ y*b->size + x ];
		
	}
	
	printf("save the grid config in a file? [Y/n] ");
	ans = fgetc( stdin );
	while ( fgetc( stdin )!='\n' ) {} // clear the rest of stdin
	
	if ( ans=='Y' || ans=='y' ) {
		char fileName[32] = {0};
		char fullFileName[38] = "saves/";
		
		printf("save file name: ");
		fgets( fileName, 32, stdin );
		for ( int i=0; i<32; i++ ) {
			if ( fileName[i]=='\n' ) { break; }
			fullFileName[i+6] = fileName[i];
		}
		// open the file
		FILE * configFile = fopen( fullFileName, "w" );
		// write the config
		char sizeBuffer[5] = {0};
		sprintf( sizeBuffer, "%d", b->size );
		for ( int i=0; i<5; i++ ) {
			if ( sizeBuffer[i] == '\0' ) { break; }
			fputc( sizeBuffer[i], configFile );
		}
		fputc( ':', configFile );
		for ( int i=0; i<b->totalsize; i++ ) {
			if ( b->grid[i]==1 ) {
				fputc('1', configFile);
			} else {
				fputc('0', configFile);
			}
		}
		// done storing the config.
		fclose( configFile );
		
	}
	
	while ( 1 ) {
		system("clear"); printBoard( b );
		update( b );
		usleep(250000);
	}
	
	return 0;
}
