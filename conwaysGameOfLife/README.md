# conwaysGameOfLife

a [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) editor.<br/>

## Usage:
#### first time:
$ make all<br/>
#### after that:
$ ./main<br/>
the program will first ask if you want to load the game of life grid<br/>
from a config file (in the saves directory).<br/>
After this, the program will go into edit mode where you can edit the<br/>
grid. In edit mode you can select coordinates to flip on or off.<br/>
When you exit edit mode, the game of life will start.<br/>

### SHA256 hashes:
a2a3249e3273aa7c854492703024755c775664d17368c82142c7ac4feb8a28f9  main.c<br/>
